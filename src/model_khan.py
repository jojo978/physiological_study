import torch
import torch.nn as nn
from torch.nn import functional as F


class SnoreModel(nn.Module):
    def __init__(self):
        super().__init__()

        # Kahn (2019) architecture
        self.conv1 = nn.Conv2d(1, 32, kernel_size=3, padding=1)
        self.conv2 = nn.Conv2d(32, 32, kernel_size=3)
        self.maxpool1 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.do1 = nn.Dropout2d(0.25)
        self.conv3 = nn.Conv2d(32, 64, kernel_size=3, padding=1)
        self.conv4 = nn.Conv2d(64, 64, kernel_size=3)
        self.maxpool2 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.do2 = nn.Dropout2d(0.25)

        # fully connected
        self.d1 = nn.Linear(2304, 512)
        self.do3 = nn.Dropout(0.5)
        self.d2 = nn.Linear(512, 64)
        self.d3 = nn.Linear(64, 1)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = self.do1(self.maxpool1(x))
        x = F.relu(self.conv3(x))
        x = F.relu(self.conv4(x))
        x = self.do2(self.maxpool2(x))
        x = F.relu(self.d1(x.view(x.size(0), -1)))
        x = F.relu(self.d2(self.do3(x)))
        x = torch.sigmoid(self.d3(x))

        return x

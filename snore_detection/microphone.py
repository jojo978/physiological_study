#!/usr/bin/env python3
"""Plot the live microphone signal(s) with matplotlib.

Matplotlib and NumPy have to be installed.

"""
import argparse
import queue
import sys
import time

from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt
import numpy as np
import sounddevice as sd

import torch
from src.model_khan import SnoreModel

from librosa.feature import mfcc


def int_or_str(text):
    """Helper function for argument parsing."""
    try:
        return int(text)
    except ValueError:
        return text


parser = argparse.ArgumentParser(add_help=False)
parser.add_argument(
    '-l', '--list-devices', action='store_true',
    help='show list of audio devices and exit')
args, remaining = parser.parse_known_args()
if args.list_devices:
    print(sd.query_devices())
    parser.exit(0)

parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter,
    parents=[parser])
parser.add_argument(
    'channels', type=int, default=[1], nargs='*', metavar='CHANNEL',
    help='input channels to plot (default: the first)')
parser.add_argument(
    '-d', '--device', type=int_or_str,
    help='input device (numeric ID or substring)')
parser.add_argument(
    '-w', '--window', type=float, default=200, metavar='DURATION',
    help='visible time slot (default: %(default)s ms)')
parser.add_argument(
    '-i', '--interval', type=float, default=30,
    help='minimum time between plot updates (default: %(default)s ms)')
parser.add_argument(
    '-b', '--blocksize', type=int, help='block size (in samples)')
parser.add_argument(
    '-r', '--samplerate', type=float, help='sampling rate of audio device')
parser.add_argument(
    '-n', '--downsample', type=int, default=10, metavar='N',
    help='display every Nth sample (default: %(default)s)')
args = parser.parse_args(remaining)

if any(c < 1 for c in args.channels):
    parser.error('argument CHANNEL: must be >= 1')
mapping = [c - 1 for c in args.channels]  # Channel numbers start with 1
q = queue.Queue()


def audio_callback(in_data, _frame, _time, status):
    """This is called (from a separate thread) for each audio block."""
    if status:
        print(status, file=sys.stderr)

    # note that in_data[:, mapping] is a copy of in_data
    q.put(in_data[:, mapping])


def update_plot(_frame):
    """This is called by matplotlib for each plot update.

    Typically, audio callbacks happen more frequently than plot updates,
    therefore the queue tends to contain multiple blocks of audio data.

    Returns:
        (list[matplotlib artists])

    """
    global plot_data, snore_count, last_snore_time

    while True:
        try:
            data = q.get_nowait()
        except queue.Empty:
            break
        shift = len(data)
        plot_data = np.roll(plot_data, -shift, axis=0)
        plot_data[-shift:, :] = data

    # mfcc features for the last second audio
    ms = 1.0e-3
    sample_rate = args.samplerate
    hop_length = int(31.27 * ms * sample_rate)
    n_fft = int(np.ceil(50 * ms * sample_rate / 512) * 512)

    # librosa.feature.mfcc expects the last index to be the time axis
    # and preserves all others
    t = mfcc(y=plot_data[:, 0], sr=sample_rate, n_mfcc=32, hop_length=hop_length, n_fft=n_fft)
    score = model(torch.tensor(t[None, None, :32, :32], requires_grad=False).float()).squeeze().item()

    if score > 0.5 and last_snore_time < time.time() - 1:
        print(f'{snore_count} Snoring!')
        snore_count += 1
        last_snore_time = time.time()

    # update line artist data
    for column, line in enumerate(lines):
        line.set_ydata(plot_data[::args.downsample, column])

    return lines


if __name__ == '__main__':
    snore_count = 0
    last_snore_time = time.time() - 1

    if args.samplerate is None:
        device_info = sd.query_devices(args.device, 'input')
        args.samplerate = device_info['default_samplerate']

    length = int(args.window * args.samplerate / 1000)
    plot_data = np.zeros((length, len(args.channels)))

    # first draw and keeps line artists
    fig, ax = plt.subplots()

    # ax.plot expects the first axis to be the time axis
    lines = ax.plot(plot_data[::args.downsample])

    if len(args.channels) > 1:
        ax.legend(['channel {}'.format(c) for c in args.channels],
                  loc='lower left', ncol=len(args.channels))
    ax.axis((0, len(plot_data[::args.downsample]), -1, 1))
    ax.set_yticks([0])
    ax.yaxis.grid(True)
    ax.tick_params(bottom=False, top=False, labelbottom=False,
                   right=False, left=False, labelleft=False)
    fig.tight_layout(pad=0)

    # load a pretrained snore classifier
    model = SnoreModel()
    model.load_state_dict(torch.load('snore_model_epoch_50.pt'))
    model.eval()

    stream = sd.InputStream(device=args.device, channels=max(args.channels),
                            samplerate=args.samplerate, callback=audio_callback)
    ani = FuncAnimation(fig, update_plot, interval=args.interval, blit=True)

    with stream:
        plt.show()

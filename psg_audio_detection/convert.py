import sys
from os import path

import numpy as np

import pyedflib
import h5py


def convert(edf_file, hdf5_file=None):
    if hdf5_file is None:
        hdf5_file = edf_file.split('/')[-1].split('.')[0] + '.h5'

    with pyedflib.EdfReader(edf_file) as f:
        header = f.getSignalHeaders()
        print(header)
        sample_rate = header[0]['sample_rate']

        n_samples = f.getNSamples()[0]
        audio_signal = f.readSignal(0, 0, n_samples)

        with h5py.File(hdf5_file, mode='w') as h_file:
            h_file.create_dataset('audio', (n_samples,), chunks=True, dtype=np.float32)
            h_file['audio'][...] = audio_signal
            h_file['audio'].attrs['sample_rate'] = sample_rate


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit(-1)

    _, edf_file, *rest = sys.argv
    convert(edf_file, hdf5_file=rest[0] if rest else None)

from os import path
import sys

import numpy as np

import pyedflib
import h5py


def convert_to_hdf5(src, dest=None):
    with pyedflib.EdfReader(src) as edf_file:
        if dest is None:
            dest = src.split('/')[-1].split('.')[0] + '.h5'

        audio_ch = 0

        header = edf_file.getSignalHeaders()
        sample_rate = header[audio_ch]['sample_rate']
        n_samples = edf_file.getNSamples()[audio_ch]
        print(header)
        audio_signals = edf_file.readSignal(audio_ch, 0, n_samples, digital=True)

        with h5py.File(dest, mode='w') as h5_file:
            h5_file.create_dataset('audio', (n_samples,), dtype=np.int16)
            h5_file['audio'][...] = audio_signals
            h5_file['audio'].attrs['sample_rate'] = sample_rate


if __name__ == '__main__':
    convert_to_hdf5(sys.argv[1])
